import { s__ } from '~/locale';

export default {
  sample: {
    text: s__('ProjectTemplates|Sample GitLab Project'),
    icon: '.template-option .icon-sample',
  },
  plainhtml: {
    text: s__('ProjectTemplates|Plain HTML'),
    icon: '.template-option .icon-plainhtml',
  },
  jekyll: {
    text: s__('ProjectTemplates|Jekyll'),
    icon: '.template-option .icon-jekyll',
  },
  gitbook: {
    text: s__('ProjectTemplates|GitBook'),
    icon: '.template-option .icon-gitbook',
  },
  kaniko: {
    text: s__('ProjectTemplates|Kaniko'),
    icon: '.template-option .icon-kaniko',
  },
  inara: {
    text: s__('ProjectTemplates|JOSS/Inara'),
    icon: '.template-option .icon-inara',
  },
};
