# frozen_string_literal: true

module ProjectTemplateTestHelper
  def all_templates
    %w[
        plainhtml jekyll gitbook kaniko inara
      ]
  end
end

ProjectTemplateTestHelper.prepend_mod
